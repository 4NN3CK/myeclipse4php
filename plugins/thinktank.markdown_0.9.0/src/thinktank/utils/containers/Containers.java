package thinktank.utils.containers;

import java.util.Arrays;
import java.util.List;

public class Containers {

	/**
	 * Cast a list into a generic-typed list.
	 * 
	 * @param <X>
	 *            the element type for the return list
	 * @param array
	 * @return A list view of array, with an element type.
	 */
	@SuppressWarnings("unchecked")
	public static <X> List<X> castList(List list) {
		return list; // yup, that's all we do
	}

	/**
	 * Cast an array into a generic-typed list.
	 * 
	 * @param <X>
	 *            the element type for the return list
	 * @param array
	 * @return A list view of array, with an element type.
	 */
	@SuppressWarnings("unchecked")
	public static <X> List<X> castArray(Object[] array) {
		List list = Arrays.asList(array);
		return list;
	}

}
