package thinktank.utils.containers;

/**
 * A closed range, ie [a, b] in R
 * 
 *
 * @author daniel
 */
public final class Range {

	/** [0,0] ??Any better ideas? */ 
	public static final Range EMPTY_RANGE = new Range(0,0);
	private final int low;
	private final int high;
	
	public int low() {
		return low;
	}
	public int high() {
		return high;
	}

	/**
	 * Create a range object. 
	 * @param a These do not need to be in order.
	 * @param b These do not need to be in order.
	 */
	public Range(int a, int b) {
		assert !Double.isNaN(a) && !Double.isNaN(b);
		if (a < b) {
			low = a; high = b;
		} else {
			low = b; high = a;
		}
	}
	
	/**
	 * Not really the union, but the smallest enclosing range. I.e the supremum
	 * of the two ranges if you like to think in lattices.
	 * @param b
	 * @return
	 */
	public Range getUnion(Range b) {
		return new Range(Math.min(low, b.low), Math.max(high, b.high));
	}
	/**
	 * 
	 * @param b
	 * @return EMPTY_RANGE if empty. 
	 */
	public Range getIntersection(Range b) {
		int l = Math.max(low, b.low);
		int h = Math.min(high, b.high);
		if (l > h) return EMPTY_RANGE;
		return new Range(l,h);
	}
	
	public boolean inRange(int x) {
		return x>= low && x <= high;
	}
	public int getLength() {
		return high - low;
	}
	
}
