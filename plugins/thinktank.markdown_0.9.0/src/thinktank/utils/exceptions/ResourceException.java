package thinktank.utils.exceptions;

/**
 * For exceptions related to missing resources. Similar to IOException.
 */
public class ResourceException extends RuntimeException {
	private static final long serialVersionUID = 1;

	/**
	 * Create a new ResourceException.
	 * @param msg
	 */
	public ResourceException(final String msg) {
		super(msg);
	}

	/**
	 * Convert a built-in Java exception into a ResourceException.
	 * @param msg
	 * @param ex This can be retrieved via {@link #getCause()}
	 */
	public ResourceException(final String msg, Exception ex) {
		super(msg, ex);
	}
	/**
	 * Convert a built-in Java exception into a ResourceException.
	 * @param ex This can be retrieved via {@link #getCause()}
	 */
	public ResourceException(Exception ex) {
		super(ex);
	}

}