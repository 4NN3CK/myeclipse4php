/**
 * Copyright ThinkTank Mathematics Ltd.
 * @author Daniel Winterstein
 * 31 Aug 2006
 */
package thinktank.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Gobble output from a stream.
 * Create then call start().
 *
 * @author Based on code by Michael C. Daconta, published in
 * http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps_p.html
 */
public final class StreamGobbler extends Thread {
    private final InputStream is;
	private StringBuffer stringBuffer;
	private volatile boolean stopFlag;

    public StreamGobbler(InputStream is) {
    	super("gobbler:"+is.toString());
    	this.is = is;
    	this.stringBuffer = new StringBuffer();
    }

    @Override
	public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            while (!stopFlag) {
            	int ich = br.read();
            	if (ich==-1) break; // End of stream
            	char ch = (char) ich;
            	stringBuffer.append(ch);
//            	if (ch==StringMethods.LINEEND.charAt(0)) Logger.report("Gobbler>" + line, Logger.DEBUG_INFO);                
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Clear the stored string.
     */
    public void clearString() {
    	stringBuffer = new StringBuffer();
    }

    /**
     * @return The string, as collected so far (i.e. since starting, or the last call to clearString).
     */
    public String getString() {
    	return stringBuffer.toString();
    }

    /**
     * Request that the thread should finish. If the thread is hung waiting for 
     * output, then this will not werk.
     */
	public void pleaseStop() {
		stopFlag = true;
	}
	
	public boolean hasText() {
		return stringBuffer.length() != 0;
	}
}
