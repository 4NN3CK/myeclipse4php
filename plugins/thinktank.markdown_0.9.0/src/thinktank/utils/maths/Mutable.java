/**
 *
 * @author Daniel Winterstein
 * 17-May-2006
 */
package thinktank.utils.maths;




/**
 * A mutable version of a basic type (ie. int, bool, float, double or String).
 * This is sometimes necessary because Integer, Double, etc. do not behave like
 * normal objects.
 *
 * These objects behave equivalently to their normal counterparts with respect
 * to {@link #equals(Object)}, {@link #hashCode()} and {@link #toString()}.
 *
 * @author Daniel Winterstein
 */
public abstract class Mutable {

	/**
	 * @see Mutable
	 */
	public static class Boolean {
		public Boolean() {
			//
		}

		public Boolean(final boolean value) {
			this.value = value;
		}

		public boolean value;

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Mutable.Boolean) {
				final Boolean m = (Mutable.Boolean) obj;
				return value == m.value;
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return value ? 2 : 3;
		}

		@Override
		public java.lang.String toString() {
			return java.lang.Boolean.toString(value);
		}
	}
	//--------------------------------------------------------------------

	/**
	 * @see Mutable
	 */
	public static class Int extends Number implements Comparable<Number> {
		public Int() {
			//
		}

		public Int(final int value) {
			this.value = value;
		}

		public int value;

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Mutable.Int) {
				final Int m = (Mutable.Int) obj;
				return value == m.value;
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return value;
		}

		@Override
		public java.lang.String toString() {
			return java.lang.Integer.toString(value);
		}

		@Override
		public double doubleValue() {
			return value;
		}

		@Override
		public float floatValue() {
			return (float) value;
		}

		@Override
		public int intValue() {
			return (int) value;
		}

		@Override
		public long longValue() {
			return (long) value;
		}

		public int compareTo(Number o) {
			int y = o.intValue();
			if (y == value) return 0;
			return y<value? 1 : -1;
		}
	}
	//--------------------------------------------------------------------

	/**
	 * @see Mutable
	 */
	public static class Float extends Number implements Comparable<Number> {
		public Float() {
			//
		}

		public Float(final float value) {
			this.value = value;
		}

		public float value;

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Mutable.Float) {
				final Float m = (Mutable.Float) obj;
				return value == m.value;
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return (new java.lang.Float(value)).hashCode();
		}

		@Override
		public java.lang.String toString() {
			return java.lang.Float.toString(value);
		}

		@Override
		public double doubleValue() {
			return value;
		}

		@Override
		public float floatValue() {
			return (float) value;
		}

		@Override
		public int intValue() {
			return (int) value;
		}

		@Override
		public long longValue() {
			return (long) value;
		}
		
		public int compareTo(Number o) {
			float y = o.floatValue();
			if (y == value) return 0;
			return y<value? 1 : -1;
		}
	}
	//--------------------------------------------------------------------

	/**
	 * @see Mutable
	 */
	public static class Double extends Number implements Comparable<Number> {
		public Double() {
			//
		}

		public Double(final double value) {
			this.value = value;
		}

		public double value;

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Mutable.Double) {
				final Double m = (Mutable.Double) obj;
				return value == m.value;
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return (new java.lang.Double(value)).hashCode();
		}

		@Override
		public java.lang.String toString() {
			return java.lang.Double.toString(value);
		}

		@Override
		public double doubleValue() {
			return value;
		}

		@Override
		public float floatValue() {
			return (float) value;
		}

		@Override
		public int intValue() {
			return (int) value;
		}

		@Override
		public long longValue() {
			return (long) value;
		}

		public int compareTo(Number o) {
			double y = o.doubleValue();
			if (y == value) return 0;
			return y<value? 1 : -1;
		}
	} // End Double
	//--------------------------------------------------------------------

	/**
	 * @see Mutable
	 */
	public static class String {
		public java.lang.String value;

		public String() {
			//
		}

		public String(final java.lang.String value) {
			this.value = value;
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Mutable.String) {
				final String ms = (Mutable.String) obj;
				return value.equals(ms.value);
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return value.hashCode();
		}

		@Override
		public java.lang.String toString() {
			return value;
		}
	}

}
