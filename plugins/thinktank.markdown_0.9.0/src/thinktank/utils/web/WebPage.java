/**
 * @author Daniel Winterstein
 * Created: 1 Sep 2006
 * Copyright ThinkTank Mathematics
 */
package thinktank.utils.web;

import java.net.URI;

import thinktank.utils.StringMethods;

/**
 * Class for creating web-pages.
 *
 * This is intended as a utility class which makes creating well-formed
 * web pages quicker and easier.
 * It is not intended for complicated usage - consider working with the DOM
 * instead.
 *
 * @status Immature
 *
 * @author Daniel Winterstein
 */
public class WebPage {

	private static final String DOCTYPE = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n";

	private String title = "";
	private StringBuilder body = new StringBuilder();
	private StringBuilder style = new StringBuilder();
	/** Link to an external stylesheet */
	private String stylesheet = "";
	
	/**
	 * A blank web page.
	 */
	public WebPage() {
		//
	}
	
	/**
	 * Create a web-page with a title and header
	 * @param title This will be the title and the main header for this page.
	 */
	public WebPage(String title) {
		setTitle(title);
		addText("<h1>"+title+"</h1>");
	}

	/**
	 * Returns the HTML for this web page.
	 */
	public String toString() {
		StringBuilder html = new StringBuilder();
		html.append(DOCTYPE);
		html.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en-GB\">\n");
		html.append(getHead());
		html.append(getBody());
		html.append("</html>");
		return html.toString();
	}

	private String getBody() {
		return "<body>\n"+body+"\n</body>\n";
	}

	/**
	 * Add text to the body of the web page.
	 * @param bodyText
	 */
	public void addText(String bodyText) {
		body.append(bodyText);
	}

	private String getHead() {
		StringBuilder head = new StringBuilder();
		head.append("<head>\n");
		head.append("<title>"+title+"</title>\n");
		head.append(stylesheet);
		head.append("<style type=\"text/css\">\n"+style+"\n</style>\n");
		head.append("</head>\n");
		return head.toString();
	}

	public String getTitle() {
		return title;
	}

	/**
	 * Set the web-page title.
	 * <p>
	 * The title is used in browser tabs, etc.
	 * It does not affect how the page is rendered.
	 * To add the title to the page as well, follow this method with a call
	 * such as:
	 * <code>page.addText("&lt;h1>"+page.getTitle()+"&lt;/h1>");</code>
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Add <br/>
	 */
	public void br() {
		addText("<br/>"+StringMethods.LINEEND);
	}

	/**
	 * Set the styling for a particular tag or class.
	 * <p>
	 * Note: These stylings are collected sequentially. 
	 * @param key E.g. "h1", "a:hover", ".thinktank", etc.
	 * @param settings E.g. "color:red; background-color:green;"
	 */
	public void setStyle(String key, String settings) {
		style.append(key +" { "+settings+" }\n");
	}
	
	public void setStyleSheet(URI styleSheet) {
		stylesheet  = "<link rel=\"stylesheet\" type=\"text/css\" " +
				"href=\""+styleSheet+"\" />\n";
	}

	public String html() {
		return toString();
	}
	
}
