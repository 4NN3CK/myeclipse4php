package thinktank.utils.containers;


/**
 * A pair of objects.
 * <p>
 * Uses:
 * 	Returning two objects from a function.
 * 
 * @param <Type> Use {@link Pair2} to store objects of different types.
 */
public final class Pair<Type> {
	public Pair(final Type A, final Type B) {
		this.first = A;
		this.second = B;
	}

	public Type first;

	public Type second;

	/**
	 * Equals if
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj.getClass() != getClass()) {
			return false;
		}
		final Pair<Type> other = (Pair<Type>) obj;
		return first.equals(other.first) && second.equals(other.second);
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = (first == null) ? 0 : first.hashCode();
		if (second != null) {
			result += PRIME * second.hashCode();
		}
		return result;
	}

	/**
	 * Swap the order of the two objects
	 */
	public void swap() {
		final Type temp = first;
		first = second;
		second = temp;
	}

	/**
	 * Create a sorted Pair. That is, a pair whose elements are ordered
	 * according to compareTo().
	 */
	public static <T extends Comparable<T>> Pair<T> sorted(final T x, final T y) {
		if (x.compareTo(y) < 0) {
			return new Pair<T>(x, y);
		}
		return new Pair<T>(y, x);
	}

	
}