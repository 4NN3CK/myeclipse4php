package thinktank.utils.exceptions;

/**
 * For when an operation fails, although the request was reasonable.
 * 
 * @author Daniel Winterstein
 */
public class FailureException extends RuntimeException {
	/**
	 * An operation has failed, although the request was reasonable.
	 */
	public FailureException() {
		//
	}
	/**
	 * An operation has failed, although the request was reasonable.
	 */	
	public FailureException(String msg) {
		super(msg);
	}
	/**
	 * An operation has thrown an exception, although the request was reasonable.
	 */		
	public FailureException(Exception ex) {
		super(ex);
	}
}