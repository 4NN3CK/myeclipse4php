package thinktank.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import thinktank.utils.exceptions.ExceptionAdapter;

/**
 * A drop-in wrapper for an {@link ExecutorService} which avoids a couple of pitfalls.
 * <p>
 * There are a couple of dangers to beware of with Executors:
 * <ul>
 * <li> They swallow exceptions! Argh!
 * <li> awaitTermination() will hang if shutdown() has not been called first.
 * </ul>
 *
 * This class does not itself implement ExecutorService, as the interface is
 * (slightly) incompatible between Java 5 and java 6.
 * <p>
 * This class makes the following changes to ExecutorService:
 * <ul>
 * <li> awaitTermination() will throw an exception if any of the submitted tasks
 * threw an Exception.
 * <li> The invokeAll() methods will throw an exception if any of the submitted
 * tasks threw an Exception.
 * <li> awaitTermination() will throw a BadLogicException if shutdown() has not
 * 		been called first. Which is much better than quietly hanging!
 * <li> All exceptions have been converted to RuntimeExceptions for ease-of-use.
 * <li> All futures from submitted tasks are stored until the executor terminates.
 * This is done in order to check them for exceptions. This may be an problem 
 * when a lot of tasks are submitted (e.g.
 * if the executor is used indefinitely). Two methods {@link #clean()} and
 * {@link #forceClean()} provide a way of clearing the memory before termination.
 * </ul>
 *
 * @author Daniel Winterstein
 */
public final class SafeExecutor {

	private final ExecutorService executor;
	private final List<Future> futures = new ArrayList<Future>();

	public SafeExecutor(ExecutorService executor) {
		this.executor = executor;
	}

	/**
	 * Blocks until all tasks have completed execution after a shutdown
     * request, or the timeout occurs, or the current thread is
     * interrupted, whichever happens first.
     *
     * @param timeout the maximum time to wait
     * @param unit the time unit of the timeout argument
     * @return <tt>true</tt> if this executor terminated and
     *         <tt>false</tt> if the timeout elapsed before termination
     *
	 * @throws (runtime wrapped) InterruptedException if interrupted while waiting.
	 * @throws BadLogicException if shutdown has not been called first.
	 * @throws (runtime wrapped) exceptions from the callables.
	 */
	public boolean awaitTermination(long timeout, TimeUnit unit) throws RuntimeException {
		// This method can only be called after a shutdown request
		if (! executor.isShutdown()) throw new RuntimeException();
		boolean result;
		try {
			result = executor.awaitTermination(timeout, unit);
		} catch (InterruptedException ex) {
			throw new ExceptionAdapter(ex);
		}
		// Clean up - maybe throwing an exception
		clean();
		// Done
		return result;
	}

	private static final Future[] FUTURES_ARRAY = new Future[0];
	
	/**
	 * Attempt to remove completed tasks from the memory.
	 * @throws RuntimeException if one of the tasks threw an exception.
	 * <p>
	 * This method removes a task from the memory before checking it for
	 * exceptions. Hence the following code (taken from #forceClean()) <i>will</i>
	 * clean out the memory:
	 * <pre><code>
	 * while(true) {
	 *    try {
	 * 		 clean();
	 *       break;
	 *    } catch (RuntimeException ex) {
	 *       // Handle ex...
	 *    }
	 * }
	 * </pre></code>
	 */
	public void clean() throws RuntimeException {
		// Test the submitted task responses for exceptions
		for(Future f : futures.toArray(FUTURES_ARRAY)) {
			if (!f.isDone()) continue; // Ignore tasks that are still running
			futures.remove(f);
			// Test for an exception thrown
			try {
				f.get();
			} catch (InterruptedException ex) {
				// Ignore
			} catch (CancellationException ex) {
				// Ignore
			} catch (ExecutionException e) {
				// Re-Throw the exception
				if (!(e.getCause() instanceof Exception)) {
					throw new RuntimeException(
							e.getClass().getName()+": "+e.getCause().getMessage());
				} 
				// Runime-Wrap if necessary
				Exception ex = (Exception) e.getCause();
				if (ex instanceof RuntimeException) throw (RuntimeException) ex;
				throw new ExceptionAdapter(ex);
			} // end try
		} // end for
	}

	/**
	 * Clean out the memory of finished tasks, ignoring any exceptions.
	 */
	public void forceClean() {
		while(true) {
			try {
				clean();
				break;
			} catch (RuntimeException ex) {
				// ignore
			}
		}
	}

	public void shutdown() {
		executor.shutdown();
	}	

	
	/**
     * Submits a value-returning task for execution and returns a
     * Future representing the pending results of the task. The
     * Future's <tt>get</tt> method will return the task's result upon
     * successful completion.
     *
     * <p>
     * If you would like to immediately block waiting
     * for a task, you can use constructions of the form
     * <tt>result = exec.submit(aCallable).get();</tt>
     *
     * <p> Note: The {@link Executors} class includes a set of methods
     * that can convert some other common closure-like objects,
     * for example, {@link java.security.PrivilegedAction} to
     * {@link Callable} form so they can be submitted.
     *
     * @param task the task to submit
     * @return a Future representing pending completion of the task
     * @throws RejectedExecutionException if the task cannot be
     *         scheduled for execution
     * @throws NullPointerException if the task is null
     */	
	public <T> Future<T> submit(Callable<T> task)
	throws RejectedExecutionException, NullPointerException
	{
		Future<T> f = executor.submit(task);
		futures.add(f);
		return f;
	}

	public <T> List<Future<T>> invokeAll(Collection<Callable<T>> arg0)
	throws ExceptionAdapter
	{
		List<Future<T>> result;
		try {
			result = executor.invokeAll(arg0);
		} catch (InterruptedException e) {
			throw new ExceptionAdapter(e);
		}
		clean();
		return result;
	}

	public <T> List<Future<T>> invokeAll(Collection<Callable<T>> arg0, long arg1, TimeUnit arg2)
	throws ExceptionAdapter {
		List<Future<T>> result;
		try {
			result = executor.invokeAll(arg0, arg1, arg2);
		} catch (InterruptedException e) {
			throw new ExceptionAdapter(e);
		}
		clean();
		return result;
	}

	public <T> T invokeAny(Collection<Callable<T>> arg0)
	throws ExecutionException
	{
		try {
			return executor.invokeAny(arg0);
		} catch (InterruptedException e) {
			throw new ExceptionAdapter(e);
		}
	}

	public <T> T invokeAny(Collection<Callable<T>> arg0, long arg1, TimeUnit arg2)
	throws InterruptedException, ExecutionException, TimeoutException
	{
		try {
			return executor.invokeAny(arg0, arg1, arg2);
		} catch (InterruptedException e) {
			throw new ExceptionAdapter(e);
		}
	}

	public boolean isShutdown() {
		return executor.isShutdown();
	}

	public boolean isTerminated() {
		return executor.isTerminated();
	}

	public List<Runnable> shutdownNow() throws ExceptionAdapter {
		List<Runnable> undone = executor.shutdownNow();
		clean();
		return undone;
	}

	public Future<?> submit(Runnable task) {
		return executor.submit(task);
	}

	public <T> Future<T> submit(Runnable task, T result) {
		return executor.submit(task, result);
	}

	public void execute(Runnable command) {
		executor.execute(command);
	}

}
