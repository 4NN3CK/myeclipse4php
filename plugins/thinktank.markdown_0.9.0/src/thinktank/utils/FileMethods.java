package thinktank.utils;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.WriteAbortedException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import thinktank.utils.exceptions.ResourceException;


/**
 * General purpose file and directory handling methods.
 * 
 * @depends Uses the environment variable TT_DATA to give access to the data
 *          directory.
 * 
 * @author daniel.winterstein
 */
public class FileMethods {




	/**
	 * Clean out a directory. <b>Warning: This deletes all files in the
	 * directory, as well as in any child-directories.</b> It does not delete
	 * the directory itself.
	 * 
	 * @param directory
	 */
	public static boolean cleanDir(File directory) {
		assert directory.isDirectory();
		boolean success = true;
		File[] files = directory.listFiles();
		for (File f : files) {
			if (f.isDirectory()) {
				success = success && cleanDir(f);
			}
			try {
				deleteFile(f);
			} catch (ResourceException ex) {
				success = false;
			}
		}
		return success;
	}

	/**
	 * Read a text file. <b>UTF-8 safe</b>.
	 * 
	 * @param file
	 * @return The text of this file.
	 * @throws ResourceException
	 *             if anything goes wrong.
	 */
	public static String file2String(File file) throws ResourceException {
		try {
			FileInputStream in = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(in, "UTF-8");
			// is this UTF-8 safe?
			return StringMethods.toString(reader);
		} catch (Exception e2) {
			throw new ResourceException(e2.getMessage(), e2);
		}
	}

	/**
	 * Read a text file.
	 * <p>
	 * This is a convenience method for
	 * {@link StringMethods#toString(java.io.Reader)} but for GzFiles
	 * 
	 * @param gzFile
	 * @return The text of this file.
	 * @throws ResourceException
	 *             if anything goes wrong (including if the file was not a gzip
	 *             file).
	 */
	public static String gzFile2String(File gzFile) throws ResourceException {
		try {
			Reader reader = gzFileToBufferedReader(gzFile);
			return StringMethods.toString(reader);
		} catch (Exception e2) {
			throw new ResourceException(e2.getMessage(), e2);
		}
	}

	/**
	 * Use this method instead of {@link File#delete()}.
	 * <p>
	 * File.delete() is buggy under Windows, and will randomly fail from time to
	 * time (c.f. <a
	 * href="http://forum.java.sun.com/thread.jspa?forumID=4&threadID=158689">
	 * http://forum.java.sun.com/thread.jspa?forumID=4&threadID=158689</a>).
	 * This method contains several work-arounds which help alleviate the bug.
	 * 
	 * @param file
	 * @throws ResourceException
	 *             if the file could not be deleted.
	 */
	public static void deleteFile(File file) throws ResourceException {
		if (file.delete()) {
			return;
		}
		// Garbage collect in the hope that this will flush the file handles
		System.gc();
		if (file.delete()) {
			return;
		}
		// Try waiting a bit
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			//
		}
		if (file.delete()) {
			return;
		}
		// Try waiting a bit longer
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			//
		}
		// Garbage collect again
		System.gc();
		if (file.delete()) {
			return;
		}
		// Give up
		throw new ResourceException("Could not delete file " + file);
	}




	/**
	 * Use NIO methods to copy a file.
	 * 
	 * @param srcFile
	 * @param destFile
	 * @throws IOException
	 */
	public static void copyFile(File srcFile, File destFile) throws IOException {
		// Create channel on the source
		FileChannel srcChannel = new FileInputStream(srcFile).getChannel();
		// Create channel on the destination
		FileChannel dstChannel = new FileOutputStream(destFile).getChannel();
		// Copy file contents from source to destination
		dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
		// Close the channels
		srcChannel.close();
		dstChannel.close();
	}

	/**
	 * The current directory (a.k.a. the working directory) is the location in
	 * the file system from where the java command was invoked.
	 */
	public static String getCurrentDir() {
		return System.getProperty("user.dir");
	}

//	/* DOES NOT WORK :(
//	 * The current directory (a.k.a. the working directory) is the location in
//	 * the file system from where the java command was invoked.
//	 */
//	public static void chdir(File dir) {
//		assert dir.isDirectory();
//		switch(getOS()) {
//		case WINDOWS:
//			Methods.runProcess("cd", dir.getAbsolutePath());
//			return;
//		default:
//			throw new NotImplementedYetException();
//		}
//	}


	/**
	 * Constants identifying the Operating System.
	 */
	public enum OS {
		WINDOWS, MAC, LINUX, UNKNOWN
	}
	
	/**
	 * @return A constant identifying the Operating System.
	 * @deprecated Try to avoid using OS specific code.
	 */
	@Deprecated
	public static OS getOS() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.startsWith("windows")) return OS.WINDOWS;
		// TODO tests for other OSs
		return OS.UNKNOWN;
	}
	
	
	/**
	 * Ignore the specified number of lines, moving the Reader forward.
	 * 
	 * @param reader
	 * @param lines
	 * @throws IOException
	 */
	public static void skipLines(BufferedReader reader, int lines)
			throws IOException {
		for (int i = 0; i < lines; i++) {
			reader.readLine();
		}
	}

	/**
	 * Recursively search for files. TODO: filtering?
	 * 
	 * @param topDir
	 * @return All the files below topDir. Excludes directories.
	 */
	public static List<File> findFiles(File topDir) {
		List<File> list = new ArrayList<File>();
		findFiles2(list, topDir);
		return list;
	}

	private static void findFiles2(List<File> list, File dir) {
		assert dir.isDirectory();
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isFile())
				list.add(file);
			else if (file.isDirectory())
				findFiles2(list, file);
		}
	}

	/**
	 * Close a reader, ignoring any exceptions that result.
	 */
	public static void close(Reader input) {
		if (input == null)
			return;
		try {
			input.close();
		} catch (IOException e) {
			// Ignore
		}
	}

	/**
	 * Close a stream, ignoring any exceptions that result.
	 */
	public static void close(InputStream in) {
		if (in == null)
			return;
		try {
			in.close();
		} catch (IOException e) {
			// Ignore
		}
	}

	/**
	 * Close a stream, ignoring any exceptions that result.
	 */
	public static void close(OutputStream out) {
		if (out == null)
			return;
		try {
			out.close();
		} catch (IOException e) {
			// Ignore
		}
	}

	/**
	 * Close a writer, ignoring any exceptions that result.
	 */
	public static void close(Writer writer) {
		if (writer == null)
			return;
		try {
			writer.close();
		} catch (IOException e) {
			// Ignore
		}
	}

	/**
	 * Write a string to file. The oppposite of {@link #file2String(File)}.
	 * Takes care of opening and closing the stream. <b>UTF-8 safe</b>.
	 * 
	 * @param text
	 * @param file
	 *            The file to write to. Any existing contents will be
	 *            over-written.
	 * @throws ResourceException
	 *             If the operation cannot be completed.
	 */
	public static void write(String text, File file) throws ResourceException {
		try {
			FileOutputStream out = new FileOutputStream(file);
			OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
			writer.write(text);
			writer.close();
			out.close();
		} catch (IOException ex) {
			throw new ResourceException(ex);
		}
	}

	/**
	 * Write a string to file. The oppposite of {@link #gzFile2String(File)}.
	 * Takes care of opening and closing the stream.
	 * 
	 * @param text
	 * @param gzFile
	 *            The file to write to. Any existing contents will be
	 *            over-written.
	 * @throws ResourceException
	 *             If the operation cannot be completed.
	 */
	public static void gzWrite(String text, File gzFile)
			throws ResourceException {
		try {
			Writer writer = gzFileToWriter(gzFile);
			writer.write(text);
			close(writer);
		} catch (IOException ex) {
			throw new ResourceException(ex);
		}
	}

	/**
	 * Assuming you have a gzipped text file, use this to return a
	 * BufferedReader so that you may call .readLine()
	 * <p>
	 * Don't forget to close the BufferedReader when you are finished with it.
	 * 
	 * @param gzFile
	 * @return
	 * @throws ResourceException
	 *             if there are any problems opening the file or during
	 *             decompression
	 * @author Sam Halliday
	 */
	public static BufferedReader gzFileToBufferedReader(File gzFile)
			throws ResourceException {
		try {
			FileInputStream gzInput = new FileInputStream(gzFile);
			GZIPInputStream input = new GZIPInputStream(gzInput);
			InputStreamReader reader = new InputStreamReader(input);
			return new BufferedReader(reader);
		} catch (Exception e) {
			throw new ResourceException(e);
		}

	}

	/**
	 * Opposite of {@link #gzFileToBufferedReader(File)}
	 * 
	 * @param gzFile
	 * @return a Writer that will write to the specified gzFile. Remember to
	 *         close the writer to make sure everything is written.
	 * @throws IOException
	 */
	public static Writer gzFileToWriter(File gzFile) throws IOException {
		FileOutputStream gzOut = new FileOutputStream(gzFile);
		GZIPOutputStream out = new GZIPOutputStream(gzOut);
		OutputStreamWriter writer = new OutputStreamWriter(out);
		return writer;
	}

	/**
	 * Convert a string so that it can be used in a filename. I.e. replace
	 * characters that are not allowed.
	 * <p>
	 * Spaces are converted to underscores. Other non-filename chars are
	 * replaced with an X. Potential directory separators (i.e. / and \) are
	 * left alone. The drive/scheme identifier ':' is converted into an X, as
	 * this is not generally valid in a filename.
	 * 
	 * @param name
	 * @return a version of name suitable for use in a filename.
	 * @warning This is a many-to-one operation, so filename(a) == filename(b)
	 *          does not imply a == b.
	 * 
	 * @see #noDirs(String)
	 */
	public static String filename(String name) {
		// Whitespace
		name = name.replace(' ', '_');
		// Remove all non-filename characters
		Matcher m = NON_FILENAME_CHARS.matcher(name);
		name = m.replaceAll("X");
		return name;
	}

	private static final Pattern NON_FILENAME_CHARS = Pattern
			.compile("[^\\w-_./\\\\]");

	/**
	 * Convert directory separators into '.' separators. Use this with
	 * {@link #filename(String)} to convert any string into a filename safe
	 * string.
	 * 
	 * @param filename
	 * @return A copy of filename in which all directory separators have been
	 *         replaced with . chars.
	 */
	public static String noDirs(String filename) {
		filename = filename.replace('/', '.');
		filename = filename.replace('\\', '.');
		return filename;
	}


}
