package thinktank.markdown.pagemodel;

import java.io.File;
import java.util.Collections;
import java.util.List;

import thinktank.markdown.pagemodel.MarkdownPage.Header;
import thinktank.utils.FileMethods;



public class MarkdownPageTest //extends TestCase 
{

	public static void main(String[] args) {
		MarkdownPageTest mpt = new MarkdownPageTest();
		mpt.testGetHeadings();
	}
	
	public void testGetHeadings() {
		// problem caused by a line beginning --, now fixed
		String txt = FileMethods.file2String(new File(
				"/home/daniel/thinktank/companies/DTC/projects/DTC-bayes/report1.txt")); 
		MarkdownPage p = new MarkdownPage(txt);
		List<Header> h1s = p.getHeadings(null);
		Header h1 = h1s.get(0);
		List<Header> h2s = h1.getSubHeaders();
		assert h2s.size() > 2;
	}

}
