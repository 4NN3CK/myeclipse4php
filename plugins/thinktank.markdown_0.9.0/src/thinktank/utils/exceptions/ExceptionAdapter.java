package thinktank.utils.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Wrap a checked exception, converting it into an unchecked one. Use this
 * instead of swallowing exceptions.
 * 
 * @author Bruce Eckel and Heinz Kabut Freely distributed via Eckel's website.
 */
public class ExceptionAdapter extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1;

	private final String stackTrace;

	public Throwable originalException;

	public ExceptionAdapter(final Throwable e) {
		this(e, null);
	}

	public ExceptionAdapter(final Throwable e, String additionalInfo) {
		super((additionalInfo == null ? e.toString() : e.toString() + " : "
				+ additionalInfo));
		originalException = e;
		final StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		stackTrace = sw.toString();
	}

	@Override
	public void printStackTrace() {
		printStackTrace(System.err);
	}

	@Override
	public void printStackTrace(final java.io.PrintStream s) {
		synchronized (s) {
			s.print(originalException.getClass().getName() + ": ");
			s.print(stackTrace);
		}
	}

	@Override
	public void printStackTrace(final java.io.PrintWriter s) {
		synchronized (s) {
			s.print(originalException.getClass().getName() + ": ");
			s.print(stackTrace);
		}
	}

	public void rethrow() throws Throwable {
		throw originalException;
	}

	/**
	 * Wrap the input exception to make it unchecked.
	 * Does nothing if the input is already unchecked.
	 */
	public static RuntimeException runtime(Throwable e) {
		if (e instanceof RuntimeException) {
			return (RuntimeException) e;
		}
		return new ExceptionAdapter(e);
	}
	/**
	 * Unwrap the input exception to get to the original cause.
	 * Does nothing if the input is already unwrapped.
	 */
	public static Throwable unwrap(Throwable e) {
		if (e.getCause() == null) {
			return e;
		}
		return unwrap(e.getCause());
	}

}