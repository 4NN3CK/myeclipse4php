/**
 * Copyright ThinkTank Mathematics Ltd.
 * @author Daniel Winterstein
 * 24 Apr 2007
 */
package thinktank.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import thinktank.utils.containers.Pair;
import thinktank.utils.exceptions.ExceptionAdapter;
import thinktank.utils.exceptions.FailureException;
import thinktank.utils.exceptions.ResourceException;

/**
 * Copy of some of ThinkTank's utils library.
 *
 * @author Daniel Winterstein
 */
public class Methods {
	
	private static String calculateOS() {
		String osName = System.getProperty("os.name").toLowerCase();
		assert osName != null;
		if (osName.startsWith("mac os x")) {
			return "apple";
		}
		if (osName.startsWith("windows")) {
			return "windows";
		}
		if (osName.startsWith("linux")) {
			return "linux";
		}
		if (osName.startsWith("sun")) {
			return "sun";
		}
		return "unknown";
	}
	/**
	 * Run a Callable with a time limit. If the call exceeds the timeout, an
	 * exception is thrown.
	 * 
	 * Usage: To safe-guard against calls which may hang or run indefinitely.
	 * 
	 * @param <T>
	 * @param method
	 *            The method to execute
	 * @param timeoutMilliSecs
	 *            The limit on how long to take
	 * @return The output from method if successful.
	 * @throws FailureException
	 *             if timed out
	 * @throws RuntimeException
	 *             if the method throws an exception
	 */
	public static <T> T timedMethod(Callable<T> method, long timeoutMilliSecs)
			throws FailureException, RuntimeException {
		assert timeoutMilliSecs > 0;
		// Submit
		ExecutorService exe = Executors.newSingleThreadExecutor();
		ExecutorService exec = exe;
//		SafeExecutor exec = new SafeExecutor(exe);
		Future<T> result = exec.submit(method);
		// Wait
		exec.shutdown();
		try {
			boolean ok = exec.awaitTermination(timeoutMilliSecs, TimeUnit.MILLISECONDS);
			if (!ok)
				throw new FailureException("Method " + method + " timed out");
			// Return
			return result.get();
		} catch (Exception e) {
			throw ExceptionAdapter.runtime(e);
		}
	}

	

	/**
	 * @param cmd
	 * @return the process for this command (started)
	 * @throws IOException
	 */
	private static Process runProcessMacLinux(String... cmd) throws IOException {
		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process process = pb.start();
		return process;
	}

	/**
	 * Handle Windows oddities
	 * 
	 * @param cmd
	 * @return
	 * @throws IOException
	 */
	private static Process runProcessWindows(String... cmd) throws IOException {
		String osName = System.getProperty("os.name");
		String[] cmdArray = new String[2 + cmd.length];
		if (osName.equals("Windows 95")) {
			cmdArray[0] = "command.com";
			cmdArray[1] = "/C";
			for (int i = 0; i < cmd.length; i++) {
				cmdArray[2 + i] = cmd[i];
			}
		} else {
			cmdArray[0] = "cmd.exe";
			cmdArray[1] = "/C";
			for (int i = 0; i < cmd.length; i++) {
				cmdArray[2 + i] = cmd[i];
			}
		}
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmdArray);
		return proc;
	}

	/**
	 * Convenience wrapper for {@link #timedMethod(Callable, long)}.
	 */
	public static void timedMethod(final Runnable method, long timeoutMilliSecs)
			throws FailureException, RuntimeException {
		Callable<Object> task = new Callable<Object>() {
			public Object call() throws Exception {
				method.run();
				return null;
			}
		};
		timedMethod(task, timeoutMilliSecs);
	}
	

	/**
	 * @see #runProcess(String...)   
	 */
	public static Pair<String> runProcess(long timeout, final String... cmd_and_args)
			throws ExceptionAdapter {
		assert timeout > 0;
		Callable<Pair<String>> rp2 = new Callable<Pair<String>>(){			
			public Pair<String> call() throws Exception {
				return runProcess(cmd_and_args);
			}		
			public String toString() {			
				return "Process: "+Arrays.toString(cmd_and_args);
			}
		};
		Pair<String> out = Methods.timedMethod(rp2, timeout);
		return out;
	}

	/**
	 * Run a command in a separate process. Blocks until the command finishes.
	 * Due to bugs in Java, we check 10 times a second until the process finishes
	 * before returning.
	 * <p>
	 * If you want to interpret shell metacharacters (such as redirection 
	 * characters and pipes) in your command, you'll need to run a shell and 
	 * pass the command to it for interpretation.
	 * E.g. Use "/bin/sh" "-c" "grep -l \"string\" *" on linux. Note that the
	 * interpretation of wild cards is really done by the shell process. 
	 * 
	 * @param cmd_and_args The command and (in separate strings) it's arguments.
	 * @return The output and error from the command.
	 * @throws IOException (runtime wrapped)
	 * @see http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4223690            
	 */
	public static Pair<String> runProcess(String... cmd_and_args) 
	{
			// Make a process
			Process process = null;
			if (calculateOS().equals("windows")) {
				try {
					process = runProcessWindows(cmd_and_args);
				} catch (IOException e) {
					ExceptionAdapter.runtime(e);
				}
			} else {
				try {
					process = runProcessMacLinux(cmd_and_args);
				} catch (IOException e) {
					ExceptionAdapter.runtime(e);
				}
			}
			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(process
					.getInputStream());
			// any error message?
			StreamGobbler errorGobbler = new StreamGobbler(process
					.getErrorStream());
			// kick them off
			errorGobbler.start();
			outputGobbler.start();
			// Wait for the process to finish
			// Note that Process.waitFor() is buggy (as of Java 6) and should 
			// not be used.
			while(true) {
				try {
					// this returns an integer if the process has exited, else
					// throws the Exception below, so we keep trying.
					process.exitValue();
					break;
				}
				catch (IllegalThreadStateException ix) {
					Methods.sleep(100); // ignore with a pause
				}
			}
			// Cleanup
			errorGobbler.pleaseStop();
			outputGobbler.pleaseStop();			
			// Read off any output
			String output = outputGobbler.getString();
			String err = errorGobbler.getString();			
			// Done
			return new Pair<String>(output, err);
	}

	

	/**
	 * Try to sleep. Catch any interruptions and return.
	 * 
	 * @param millisecs
	 */
	public static void sleep(long millisecs) {
		try {
			Thread.sleep(millisecs);
		} catch (InterruptedException e) {
			// Ignore
		}
	}
}
